"use strict";

const Fse = require('fs-extra');
const Chokidar = require('chokidar');
const Commander = require('commander');
const Path = require('path');
const uuid = require('uuid/v4');

function blockCpuFor(ms) {
    let now = new Date().getTime();
    let result = 0;
    
    while (true) {
        result += Math.random() * Math.random();
        if (new Date().getTime() > now +ms)
            return;
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

async function handleDirty(path){
    console.info(`${Date.now()}::!DIRTY!::${path}`);
    await Fse.remove(path);
    console.info(`${Date.now()}::!REMOVED!::${path}`);
}

async function handleClean(path){
    console.info(`${Date.now()}::!CLEAN!::${path}`);
}

async function doWork(path){
    let rand = getRandomInt(4);
    
    blockCpuFor(1000 * Math.random());
    
    switch (rand) {
        case 0: // Always clean
            await handleClean(path);
            break;
            
        case 1: // Always dirty
            await handleDirty(path);
            break;
            
        case 2: // ext based logic
            let fileExt = Path.extname(path);
            if (fileExt === '.dirty')
                await handleDirty(path);
            else
                await handleClean(path);
            break;

        case 3:
            console.error(`${Date.now()}::!ERROR!::${path}`);
            break;
    }
}

const args = Commander
    .usage('[options]')
    .description(`This is the application you are require to test.
    The application is watching changes to a specified folder and look for dirty files to delete. 
    The application identifies a dirty file by it's file extension '.dirty'. 
    Application output is to console (STDOUT) and will follow this format 'TIME::!ACTION!::PATH'.
    Possible actions are [ADDED,CHANGED,DIRTY,REMOVED,CLEAN,ERROR]. 
    In order to generate test files you can use the -g [number] to create 50% dirty and 50% clean files.`)
    .option('-w, --watch <string>', 'Path to a folder to watch')
    .option('-g, --generate-data <int>', 'Number of test files to generate, in the path given', x => parseInt(x, 10))
    .option('-o, --output <string>', 'Path to output folder for generated files')
    .parse(process.argv);

if (!((args.watch && Fse.pathExistsSync(args.watch)) || (args.generateData && args.output))) {
    Commander.help();
    process.exit(-1);
}

if (args.generateData) {
    const count = args.generateData;
    const outputPath = args.output;
    
    let cleanPath = Path.join(outputPath, 'clean');
    Fse.ensureDirSync(cleanPath);

    let dirtyPath = Path.join(outputPath, 'dirty');
    Fse.ensureDirSync(dirtyPath);

    for (let i = 1; i <= count; i++) {
        if ((i % 2) === 0)
            Fse.ensureFileSync(Path.join(cleanPath,`${uuid()}.clean`));
        else
            Fse.ensureFileSync(Path.join(dirtyPath,`${uuid()}.dirty`));
    }

    console.log(`#${count} files were generated under ${outputPath}`);
}

if (args.watch) {
    let watcher = Chokidar.watch(args.watch, {
        ignoreInitial: true
    });
    watcher.on('add', (path, stats) => {
        console.info(`${Date.now()}::!ADDED!::${path}`);
        doWork(path);
    });
    watcher.on('change', (path, stats) => {
        console.info(`${Date.now()}::!CHANGED!::${path}`);
        doWork(path);
    });

    console.log(`Watching the following path "${args.watch}"`);
}
